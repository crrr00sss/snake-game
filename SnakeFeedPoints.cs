﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project2
{
    class SnakeFeedPoint
    {
        readonly Random _random;
        const char BODY_ELEMENT = (char)0x25A0;
        readonly ConsoleColor _color;
        int _prevX, _prevY;

        public int X { get; set; }
        public int Y { get; set; }

        public int ScoreWeight { get; set; }

        public SnakeFeedPoint(ConsoleColor color, int scoreWeight)
        {
            _random = new();
            _color = color;

            ScoreWeight = scoreWeight;
        }

        public void GetRandomCoordinates()
        {
            X = _random.Next(0, View.ScreenWidth - 3);
            Y = _random.Next(0, View.ScreenHeight - 3);
        }

        public void ShowFeed()
        {
            View.PrintElement(_prevX, _prevY, ' ');

            View.PrintElement(X, Y, BODY_ELEMENT, _color);

            _prevX = X;
            _prevY = Y;
        }

    }
}
