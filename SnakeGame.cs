﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Project2
{
    class SnakeGame
    {
        Snake _snake;
        SnakeFeedPoint _snakeFeed, _snakeFeedExtra;
        int _score;
        bool _isExit;

        public bool IsGoToMenu { get; set; }

        public int GameSpeed { get; set; } = 200;

        public bool SnakeGoThroughWall { get; set; } = false;

        public bool MusicOn { get; set; } = true;

        public bool ExtraFeed { get; set; } = false;

        public void SetStartSettings()
        {
            _isExit = false;

            IsGoToMenu = false;

            _snake = new(GameSpeed);
            _snakeFeed = new(ConsoleColor.Red, 1);

            if (ExtraFeed)
            {
                _snakeFeedExtra = new(ConsoleColor.Green, 2);
            }

            View.PrintCurrentGameScoreOnScreenTitle(_snake.Speed, _score);
        }

        public void Start()
        {
            SetStartSettings();

            View.ClearScreen();

            Thread _directionChanged_thread = new(SetUserDirection);
            _directionChanged_thread.Start();

            Thread _playGame_thread = new(ShowSnakeAndFeeds);
            _playGame_thread.Start();

            Thread _playBackgroundMusic_thread = new(Music.PlayBackgroundMusic);

            if (MusicOn)
            {
                _playBackgroundMusic_thread.Start(_playGame_thread);
            }


        }

        void SnakeMoving()
        {
            _snake.Move(_snake.SnakeDirection);

            if (IsSnakeGetFeed(_snakeFeed))
            {
                _snake.IncreaseSnakeLenght();

                _score += _snakeFeed.ScoreWeight;

                View.PrintCurrentGameScoreOnScreenTitle(_snake.Speed, _score);
            }
            else if (IsSnakeGetFeed(_snakeFeedExtra))
            {
                _snake.IncreaseSnakeLenght();

                _score += _snakeFeedExtra.ScoreWeight;

                View.PrintCurrentGameScoreOnScreenTitle(_snake.Speed, _score);
            }
        }

        void SetUserDirection()
        {
            bool is_Q_forExit = false;

            while (!_isExit)
            {

                ConsoleKey c = KeyController.GetUserKey();

                is_Q_forExit = c == ConsoleKey.Q;

                if (!is_Q_forExit)
                {

                    Direction d = c switch
                    {
                        ConsoleKey.UpArrow => Direction.Up,
                        ConsoleKey.DownArrow => Direction.Down,
                        ConsoleKey.RightArrow => Direction.Right,
                        ConsoleKey.LeftArrow => Direction.Left,
                        _ => Direction.Default
                    };

                    if (d == Direction.Default)
                    {
                        continue;
                    }

                    _snake.SnakeDirection = d;

                }

                else
                {
                    _isExit = true;
                }
            }

            if (is_Q_forExit)
            {
                KeyController.WaitForUserReaction();
            }

            IsGoToMenu = true;

        }

        void ShowFeed(object snakeFeedPoint)
        {
            if (snakeFeedPoint is SnakeFeedPoint snakeFeed)
            {
                snakeFeed.GetRandomCoordinates();
                snakeFeed.ShowFeed();
            }
        }

        bool IsSnakeGetFeed(SnakeFeedPoint snakeFeedPoint)
        {
            bool isSnakeGetFeed = _snake[0].X == snakeFeedPoint?.X && _snake[0].Y == snakeFeedPoint?.Y;

            return isSnakeGetFeed;
        }

        void ExitGame()
        {
            _isExit = true;

            View.PrintGameOver(_score);

            _score = 0;
        }

        void ShowSnakeAndFeeds()
        {

            int snakeMovedTimes = 0;

            while (!_isExit)
            {
                snakeMovedTimes++;

                if (snakeMovedTimes % 30 == 0)
                {
                    ShowFeed(_snakeFeed);
                }

                if (snakeMovedTimes % 14 == 0)
                {
                    ShowFeed(_snakeFeedExtra);
                }

                SnakeMoving();

                SetExitIfSnakeDead();
            }

            ExitGame();
        }

        void SetExitIfSnakeDead()
        {
            if (!_snake.SnakeHasUniqueBodyPoints())
            {
                _isExit = true;
            }
            else if (!SnakeGoThroughWall)
            {
                Point snakeHead = _snake[0];

                bool isSnakeGetRight = snakeHead.X == View.ScreenWidth - 1;
                bool isSnakeGetLeft = snakeHead.X == -1;
                bool isSnakeGetBottom = snakeHead.Y == View.ScreenHeight - 1;
                bool isSnakeGetUp = snakeHead.Y == -1;

                _isExit = _isExit || isSnakeGetRight || isSnakeGetLeft || isSnakeGetBottom || isSnakeGetUp;
            }
        }
    }
}
