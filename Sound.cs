﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    struct Sound
    {
        public int Frequency { get; init; }
        public int Duration { get; init; }
    }
}
