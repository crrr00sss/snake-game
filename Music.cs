﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project2
{
    static class Music
    {
        readonly static Sound[] _musicSounds =
            {
                new Sound{Frequency = 1320, Duration = 500},
                new Sound { Frequency = 990, Duration = 250 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 1188, Duration = 250 },
                new Sound { Frequency = 1320, Duration = 125 },
                new Sound { Frequency = 1188, Duration = 125 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 990, Duration = 250 },
                new Sound { Frequency = 880, Duration = 500 },
                new Sound { Frequency = 880, Duration = 250 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 1320, Duration = 500 },
                new Sound { Frequency = 1188, Duration = 250 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 990, Duration = 750 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 1188, Duration = 500 },
                new Sound { Frequency = 1320, Duration = 500 },
                new Sound { Frequency = 1056, Duration = 500 },
                new Sound { Frequency = 880, Duration = 500 },
                new Sound { Frequency = 880, Duration = 500 },
                new Sound { Frequency = 0, Duration = 250 },
                new Sound { Frequency = 1188, Duration = 500 },
                new Sound { Frequency = 1408, Duration = 250 },
                new Sound { Frequency = 1760, Duration = 500 },
                new Sound { Frequency = 1584, Duration = 250 },
                new Sound { Frequency = 1408, Duration = 250 },
                new Sound { Frequency = 1320, Duration = 750 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 1320, Duration = 500 },
                new Sound { Frequency = 1188, Duration = 250 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 990, Duration = 500 },
                new Sound { Frequency = 990, Duration = 250 },
                new Sound { Frequency = 1056, Duration = 250 },
                new Sound { Frequency = 1188, Duration = 500 },
                new Sound { Frequency = 1320, Duration = 500 },
                new Sound { Frequency = 1056, Duration = 500 },
                new Sound { Frequency = 880, Duration = 500 },
                new Sound { Frequency = 880, Duration = 500 },
                new Sound { Frequency = 0, Duration = 500 }
            };

        public static void PressedKey()
        {
            Console.Beep(990, 250);
        }

        public static void PlayBackgroundMusic(object oMainThread)
        {
            Thread mainThread = oMainThread as Thread;

            while (mainThread.IsAlive)
            {
                foreach (var sound in _musicSounds)
                {
                    if (mainThread.IsAlive == false)
                    {
                        break;
                    }

                    if (sound.Frequency != 0)
                    {
                        Console.Beep(sound.Frequency, sound.Duration);
                    }
                    else
                    {
                        Thread.Sleep(sound.Duration);
                    }
                }
            }
        }
    }
}
