﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class MenuTreeItem
    {
        public MenuTreeItem Parent { get; init; }
        public string ConsoleName { get; init; } = string.Empty;
        public bool IsReturn { get; init; } = false;
        public Action<object> Action;

    }
}
