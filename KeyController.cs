﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class KeyController
    {
        public static ConsoleKey GetUserKey()
        {
            ConsoleKey key = Console.ReadKey(true).Key;

            if (Menu.PressedKeyMusic) Music.PressedKey();

            return key;
        }

        public static void WaitForUserReaction()
        {
            Console.ReadKey();
        }

        public static void ListenUser()
        {
            Menu menu = new();

            menu.ChooseMenuItem();
        }
    }
}
