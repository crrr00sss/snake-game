﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class View
    {
        public static void SetWindowSettings()
        {
            Console.WindowHeight = 20;
            Console.BufferHeight = Console.WindowHeight;

            Console.WindowWidth = 50;
            Console.BufferWidth = Console.WindowWidth;

            Console.CursorVisible = false;
        }

        public static void PrintMenu(IEnumerable<MenuTreeItem> menu, int choosenMenu)
        {
            Console.Clear();
            Console.CursorTop = 0;
            Console.CursorLeft = 0;

            Console.Title = menu.ElementAt(0).Parent.ConsoleName;

            for (int i = 0; i < menu.Count(); i++)
            {
                if (i == choosenMenu)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Black;
                }

                Console.WriteLine(menu.ElementAt(i).ConsoleName);
            }

            Console.ResetColor();

        }

        public static void PrintElement(int x, int y, char element, ConsoleColor color = ConsoleColor.White)
        {
            Console.CursorLeft = x;
            Console.CursorTop = y;

            Console.ForegroundColor = color;

            Console.Write(element);

            Console.ResetColor();
        }

        public static void PrintGameOver(int score)
        {
            Console.Clear();

            Console.WriteLine("Game over");

            Console.WriteLine($"Your score is {score}");

            Console.WriteLine("Press any key...");

        }

        public static void PrintExit()
        {
            Console.Clear();

            Console.WriteLine("Game Exit");

            Console.ReadKey();
        }

        public static (int X, int Y) GetScreenCentroid()
        {
            return (ScreenWidth / 2, ScreenHeight / 2);
        }

        public static int ScreenWidth { get; } = Console.WindowWidth;
        public static int ScreenHeight { get; } = Console.WindowHeight;

        public static void PrintCurrentGameScoreOnScreenTitle(int snakeSpeed, int gameScore)
        {
            Console.Title = $"Snake: Speed = {snakeSpeed} Score = {gameScore}";
        }

        public static void ClearScreen()
        {
            Console.Clear();
        }
    }
}
