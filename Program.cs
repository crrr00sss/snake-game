﻿using System;
using System.Threading;

namespace Project2
{
    class Program
    {
        static void Main()
        {
            View.SetWindowSettings();

            Thread controllerThread = new(KeyController.ListenUser);

            controllerThread.Start();
            controllerThread.Join();

            View.PrintExit();
        }
    }
}
