﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Project2
{
    class Snake
    {
        const char BODY_ELEMENT = (char)0x25A0;
        Point[] _points;
        Direction _snakeDirection;
        int _speed; //ms


        public Snake(int speed)
        {
            GetStartSnakePoints();

            Speed = speed;

            _snakeDirection = Direction.Right;
        }

        public Point this[int index] => _points[index];

        public int SnakeLenght => _points.Length;

        public Direction SnakeDirection
        {
            get => _snakeDirection;
            set => _snakeDirection = IsMovable(value) ? value : _snakeDirection;
        }

        public int Speed
        {
            get => _speed;
            set => _speed = _speed < 0 ? 0 : value;
        }

        void ShowSnake()
        {
            for (int i = 0; i < _points.Length; i++)
            {
                SetCoordIfEndWindow(ref _points[i]);
                View.PrintElement(_points[i].X, _points[i].Y, BODY_ELEMENT);
            }
        }

        public void Move(Direction direction)
        {

            ClearSnake();

            Point[] snakeMoves = new Point[_points.Length];
            snakeMoves[0] = _points[0];

            Array.Copy(_points, 0, snakeMoves, 1, _points.Length - 1);
            MovePoint(direction, ref snakeMoves[0]);

            _points = snakeMoves;

            ShowSnake();

            Thread.Sleep(Speed);
        }

        bool IsMovable(Direction direction)
        {
            bool isMovable;

            switch (direction)
            {
                case Direction.Up when _points[0].Direction == Direction.Down:
                case Direction.Down when _points[0].Direction == Direction.Up:
                case Direction.Left when _points[0].Direction == Direction.Right:
                case Direction.Right when _points[0].Direction == Direction.Left:
                    isMovable = false;
                    break;
                default:
                    isMovable = true;
                    break;
            }

            return isMovable;
        }


        static void MovePoint(Direction direction, ref Point point)
        {
            switch (direction)
            {
                case Direction.Up:
                    point.Y--;
                    break;
                case Direction.Down:
                    point.Y++;
                    break;
                case Direction.Right:
                    point.X++;
                    break;
                case Direction.Left:
                    point.X--;
                    break;
            }

            point.Direction = direction;
        }

        static void SetCoordIfEndWindow(ref Point point)
        {
            if (point.X == View.ScreenWidth)
            {
                point.X = 0;
            }
            else if (point.X == -1)
            {
                point.X = View.ScreenWidth - 1;
            }

            if (point.Y == View.ScreenHeight)
            {
                point.Y = 0;
            }
            else if (point.Y == -1)
            {
                point.Y = View.ScreenHeight - 1;
            }
        }


        void ClearSnake()
        {
            foreach (var point in _points)
            {
                View.PrintElement(point.X, point.Y, ' ');
            }
        }

        public void IncreaseSnakeLenght()
        {
            int x, y;

            switch (_points[^1].Direction)
            {
                case Direction.Up:
                    x = _points[^1].X;
                    y = _points[^1].Y + 1;
                    break;
                case Direction.Down:
                    x = _points[^1].X;
                    y = _points[^1].Y - 1;
                    break;
                case Direction.Right:
                    x = _points[^1].X - 1;
                    y = _points[^1].Y;
                    break;
                case Direction.Left:
                    x = _points[^1].X + 1;
                    y = _points[^1].Y;
                    break;
                default:
                    x = 0;
                    y = 0;
                    break;

            }

            Array.Resize(ref _points, _points.Length + 1);

            _points[^1] = new Point(x, y, _points[^2].Direction);

            IncreaseSpeed();
        }

        void IncreaseSpeed()
        {
            switch (_points.Length)
            {
                case 5:
                case 10:
                case 15:
                case 20:
                    Speed -= 20;
                    break;
            }

        }

        public void GetStartSnakePoints()
        {
            var screenCenter = View.GetScreenCentroid();

            int start_X = screenCenter.X;
            int start_Y = screenCenter.Y;

            _points = new Point[3];

            _points[0] = new(start_X, start_Y, Direction.Right);
            _points[1] = new(start_X - 1, start_Y, Direction.Right);
            _points[2] = new(start_X - 2, start_Y, Direction.Right);
        }

        public bool SnakeHasUniqueBodyPoints()
        {
            var groupNotUniquePointQuery = from p in _points
                                           group p by new { p.X, p.Y } into groupP
                                           where groupP.Count() > 1
                                           select groupP;

            bool isUniquePoints = !groupNotUniquePointQuery.Any();

            return isUniquePoints;
        }

    }
}
