﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    enum GameSpeed
    {
        ms50 = 50,
        ms100 = 100,
        ms200 = 200,
        ms300 = 300,
        ms400 = 400
    }
}
