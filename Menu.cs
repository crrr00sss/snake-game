﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    class Menu
    {
        MenuTreeItem[] _menuCurrent;
        int _choosenMenuPosition;
        MenuTreeItem _currentMenuItem;

        static bool s_isExit;

        readonly SnakeGame _snakeGame;
        MenuTreeItem[] _menuTreeItems;

        public static bool PressedKeyMusic { get; set; }

        void ChoseMenuPosition(int value)
        {
            if (value < 0)
            {
                _choosenMenuPosition = 0;
            }
            else if (value > _menuCurrent.Length - 1)
            {
                _choosenMenuPosition = _menuCurrent.Length - 1;
            }
            else
            {
                _choosenMenuPosition = value;
            }
        }

        public MenuTreeItem this[int index] => _menuCurrent.ElementAt(index);

        public int Lenght => _menuCurrent.Length;

        public Menu()
        {
            InitMenuArray();

            _snakeGame = new();

            s_isExit = false;

            PressedKeyMusic = true;

            _currentMenuItem = _menuTreeItems.ElementAt(0);

            SelectMenu();

            _choosenMenuPosition = 0;
        }

        public void ChooseMenuItem()
        {
            ConsoleKey consoleKeyPressed;

            while (!s_isExit)
            {
                View.PrintMenu(_menuCurrent,_choosenMenuPosition);

                consoleKeyPressed = KeyController.GetUserKey();

                if (consoleKeyPressed == ConsoleKey.UpArrow)
                {
                    ChoseMenuPosition(_choosenMenuPosition - 1);
                }
                else if (consoleKeyPressed == ConsoleKey.DownArrow)
                {
                    ChoseMenuPosition(_choosenMenuPosition + 1);
                }
                else if (consoleKeyPressed == ConsoleKey.Enter)
                {
                    EnterPressed();
                }
                else if (consoleKeyPressed == ConsoleKey.Escape)
                {
                    s_isExit = true;
                }
                else
                {
                    continue;
                }

            }
        }

        private void SelectMenu()
        {
            var query = from item in _menuTreeItems
                        where item.ConsoleName != "Snake" && item.Parent.ConsoleName == _currentMenuItem.ConsoleName
                        select item;

            _menuCurrent = query.ToArray();
        }

        private void EnterPressed()
        {
            _currentMenuItem = _menuCurrent.ElementAt(_choosenMenuPosition);

            _currentMenuItem.Action?.Invoke(_snakeGame);

            if (_currentMenuItem.IsReturn)
            {
                _currentMenuItem = _currentMenuItem.Parent.Parent ?? _menuTreeItems.ElementAt(0);
            }

            _choosenMenuPosition = 0;
            SelectMenu();
        }

        void InitMenuArray()
        {
            _menuTreeItems = new MenuTreeItem[20];

            _menuTreeItems[0] = new() { ConsoleName = "Snake" };
            _menuTreeItems[1] = new() { Parent = _menuTreeItems[0], ConsoleName = "New Game", IsReturn = true, Action = (_snakeGame) => { ((SnakeGame)_snakeGame).Start(); while (!((SnakeGame)_snakeGame).IsGoToMenu) { } } };
            _menuTreeItems[2] = new() { Parent = _menuTreeItems[0], ConsoleName = "Game Type" };
            _menuTreeItems[3] = new() { Parent = _menuTreeItems[0], ConsoleName = "Exit", Action = (_snakeGame) => s_isExit = true };

            _menuTreeItems[4] = new() { Parent = _menuTreeItems[2], ConsoleName = "Speed" };
            _menuTreeItems[5] = new() { Parent = _menuTreeItems[2], ConsoleName = "Go Throught Wall" };
            _menuTreeItems[6] = new() { Parent = _menuTreeItems[2], ConsoleName = "Food Types" };
            _menuTreeItems[7] = new() { Parent = _menuTreeItems[2], ConsoleName = "Music" };
            _menuTreeItems[8] = new() { Parent = _menuTreeItems[2], ConsoleName = "Return", IsReturn = true };

            _menuTreeItems[9] = new() { Parent = _menuTreeItems[4], ConsoleName = "400 ms", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).GameSpeed = (int)GameSpeed.ms400 };
            _menuTreeItems[10] = new() { Parent = _menuTreeItems[4], ConsoleName = "300 ms", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).GameSpeed = (int)GameSpeed.ms300 };
            _menuTreeItems[11] = new() { Parent = _menuTreeItems[4], ConsoleName = "200 ms", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).GameSpeed = (int)GameSpeed.ms200 };
            _menuTreeItems[12] = new() { Parent = _menuTreeItems[4], ConsoleName = "100 ms", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).GameSpeed = (int)GameSpeed.ms100 };
            _menuTreeItems[13] = new() { Parent = _menuTreeItems[4], ConsoleName = "50 ms", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).GameSpeed = (int)GameSpeed.ms50 };

            _menuTreeItems[14] = new() { Parent = _menuTreeItems[5], ConsoleName = "Yes", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).SnakeGoThroughWall = true };
            _menuTreeItems[15] = new() { Parent = _menuTreeItems[5], ConsoleName = "No", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).SnakeGoThroughWall = false };

            _menuTreeItems[16] = new() { Parent = _menuTreeItems[6], ConsoleName = "With Extra Feed", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).ExtraFeed = true };
            _menuTreeItems[17] = new() { Parent = _menuTreeItems[6], ConsoleName = "Without Extra Feed", IsReturn = true, Action = (_snakeGame) => ((SnakeGame)_snakeGame).ExtraFeed = false };

            _menuTreeItems[18] = new() { Parent = _menuTreeItems[7], ConsoleName = "Yes", IsReturn = true, Action = (_snakeGame) => { ((SnakeGame)_snakeGame).MusicOn = true; PressedKeyMusic = true; } };
            _menuTreeItems[19] = new() { Parent = _menuTreeItems[7], ConsoleName = "No", IsReturn = true, Action = (_snakeGame) => { ((SnakeGame)_snakeGame).MusicOn = false; PressedKeyMusic = false; } };

        }
    }
}
